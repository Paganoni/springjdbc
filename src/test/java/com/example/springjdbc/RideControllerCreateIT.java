package com.example.springjdbc;


import com.example.springjdbc.model.Ride;
import lombok.SneakyThrows;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.UUID;

class RideControllerCreateIT extends BaseSpringIT {
    private static final String API_URL_SIMPLE = "/api/v1/ride/update-method";
    private static final String API_URL_JDBC_INSERT = "/api/v1/ride/jdbc-insert";
    private static final String API_URL_PREPARED_STATEMENT = "/api/v1/ride/prepared-statement";
    private static final String API_URL_RETURNED_BODY = "/api/v1/ride/with-return";

    @Test
    @SneakyThrows
    void shouldBeOkWithSimpleUpdateMethod() {
        Ride rideAlbosaggia = Ride.builder()
                .name("Albosaggia trail")
                .duration(120)
                .build();

        doRequest(MockMvcRequestBuilders.put(API_URL_SIMPLE), rideAlbosaggia)
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @SneakyThrows
    void shouldBeOkWithJdbcInsert() {
        Ride rideAlbosaggia = Ride.builder()
                .name("Albosaggia trail")
                .duration(120)
                .build();

        doRequest(MockMvcRequestBuilders.put(API_URL_JDBC_INSERT), rideAlbosaggia)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.notNullValue(UUID.class)));
    }

    @Test
    @SneakyThrows
    void shouldBeOkWithPreparedMethod() {
        Ride rideAlbosaggia = Ride.builder()
                .name("Albosaggia trail")
                .duration(120)
                .build();

        doRequest(MockMvcRequestBuilders.put(API_URL_PREPARED_STATEMENT), rideAlbosaggia)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.notNullValue(UUID.class)));
    }

    @Test
    @SneakyThrows
    void shouldBeOkWithReturnedBody() {
        Ride rideAlbosaggia = Ride.builder()
                .name("Albosaggia trail")
                .duration(120)
                .build();

        doRequest(MockMvcRequestBuilders.post(API_URL_RETURNED_BODY), rideAlbosaggia)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.notNullValue(UUID.class)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Albosaggia trail"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.duration").value(120));
    }


}
