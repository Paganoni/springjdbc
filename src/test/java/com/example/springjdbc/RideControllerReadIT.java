package com.example.springjdbc;


import com.example.springjdbc.model.Ride;
import com.example.springjdbc.repository.RideRepository;
import lombok.SneakyThrows;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.UUID;

class RideControllerReadIT extends BaseSpringIT {
    private static final String API_URL_RESULT_SET = "/api/v1/rides/row-mapper";
    private static final String API_URL_ID = "/api/v1/rides/{id}";

    @Autowired
    RideRepository rideRepository;

    @Test
    @SneakyThrows
    void shouldBeOkWithPreparedMethod() {
        Ride rideAlbosaggia = Ride.builder()
                .name("Albosaggia trail")
                .duration(120)
                .build();
        rideRepository.createRideWithUpdateMethod(rideAlbosaggia);

        doRequest(MockMvcRequestBuilders.get(API_URL_RESULT_SET), rideAlbosaggia)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*]", Matchers.hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].id", Matchers.notNullValue(UUID.class)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].name").value("Albosaggia trail"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].duration").value(120))
        ;
    }

    @Test
    @SneakyThrows
    void shouldBeOkWithSingleObject() {
        Ride rideAlbosaggia = Ride.builder()
                .name("Albosaggia trail")
                .duration(120)
                .build();
        UUID rideWithPreparedStatement = rideRepository.createRideWithPreparedStatement(rideAlbosaggia);

        doRequest(MockMvcRequestBuilders.get(API_URL_ID, rideWithPreparedStatement), null)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.notNullValue(UUID.class)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Albosaggia trail"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.duration").value(120))
        ;
    }


}
