package com.example.springjdbc;


import com.example.springjdbc.model.Ride;
import lombok.SneakyThrows;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

class RideControllerDeleteIT extends BaseSpringIT {
    private static final String API_URL_DELETE = "/api/v1/rides/{id}";
    private static final String API_URL_DELETE_NAMED = "/api/v1/rides/{id}/namedParameter";

    @Test
    @SneakyThrows
    void shouldDelete() {
        Ride rideAlbosaggia = Ride.builder()
                .name("Albosaggia trail")
                .duration(120)
                .build();
        Ride saved = rideRepository.createRideWithReturnBody(rideAlbosaggia);

        doRequest(MockMvcRequestBuilders.delete(API_URL_DELETE, saved.getId()), null)
                .andExpect(MockMvcResultMatchers.status().isOk());

        Assertions.assertThat(rideRepository.getRidesWithExternalRowMapper()).isEmpty();
    }

    @Test
    @SneakyThrows
    void shouldDeleteWithNamedParameter() {
        Ride rideAlbosaggia = Ride.builder()
                .name("Albosaggia trail")
                .duration(120)
                .build();
        Ride saved = rideRepository.createRideWithReturnBody(rideAlbosaggia);

        doRequest(MockMvcRequestBuilders.delete(API_URL_DELETE_NAMED, saved.getId()), null)
                .andExpect(MockMvcResultMatchers.status().isOk());

        Assertions.assertThat(rideRepository.getRidesWithExternalRowMapper()).isEmpty();
    }


}
