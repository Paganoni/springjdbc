package com.example.springjdbc;

import lombok.SneakyThrows;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

class RideControllerExceptionIT extends BaseSpringIT {
    private static final String API_URL_EXCEPTION = "/api/v1/testException";

    @Test
    @SneakyThrows
    void shouldTestException() {
        doRequest(MockMvcRequestBuilders.put(API_URL_EXCEPTION), null)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.containsString("bad SQL grammar")));

    }

}
