package com.example.springjdbc;

import com.example.springjdbc.repository.RideRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

@ActiveProfiles("TEST")
@SpringBootTest(classes = SpringJdbcApplication.class)
@AutoConfigureMockMvc
@ContextConfiguration(initializers = {BaseSpringIT.SpringContextInitializer.class})
@Testcontainers
@Slf4j
public abstract class BaseSpringIT {
    private static final DockerImageName postgresImage = DockerImageName.parse("postgres:13");

    public static PostgreSQLContainer postgreSQLContainer;

    @Autowired
    protected ObjectMapper objectMapper;
    @Autowired
    protected MockMvc mvc;
    @Autowired
    protected JdbcTemplate jdbcTemplate;
    @Autowired
    public RideRepository rideRepository;

    static {
        postgreSQLContainer = new PostgreSQLContainer<>(postgresImage)
                .withDatabaseName("qo-backend")
                .withUsername("qo-backend")
                .withPassword("qo-backend");
        postgreSQLContainer.start();
    }


    @AfterEach
    void tearDown() {
        cleanDatabase();
    }

    private void cleanDatabase() {
        rideRepository.deleteAll();
    }


    static class SpringContextInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        @Override
        @SneakyThrows
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            ConfigurableEnvironment environment = configurableApplicationContext.getEnvironment();

            TestPropertyValues.of(
                    "spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
                    "spring.datasource.username=" + postgreSQLContainer.getUsername(),
                    "spring.datasource.password=" + postgreSQLContainer.getPassword()
            ).applyTo(environment);
        }

    }

    protected ResultActions doRequest(MockHttpServletRequestBuilder requestWithUrl, Object body) throws Exception {
        MockHttpServletRequestBuilder requestBuilder = requestWithUrl.contentType(MediaType.APPLICATION_JSON);

        if (body != null) {
            if (body instanceof String) {
                requestBuilder = requestBuilder.content(body.toString());
            } else {
                requestBuilder = requestBuilder.content(objectMapper.writeValueAsString(body));
            }
        }

        return mvc.perform(requestBuilder);
    }
}
