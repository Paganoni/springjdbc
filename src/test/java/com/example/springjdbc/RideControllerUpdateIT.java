package com.example.springjdbc;


import com.example.springjdbc.model.Ride;
import lombok.SneakyThrows;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

class RideControllerUpdateIT extends BaseSpringIT {
    private static final String API_URL_UPDATE = "/api/v1/ride";
    private static final String API_URL_UPDATE_BATCH = "/api/v1/batch";

    @Test
    @SneakyThrows
    void shouldBeOkWithSimpleUpdateMethod() {
        Ride rideAlbosaggia = Ride.builder()
                .name("Albosaggia trail")
                .duration(120)
                .build();
        Ride saved = rideRepository.createRideWithReturnBody(rideAlbosaggia);

        Ride rideToUpdate = Ride.builder()
                .id(saved.getId())
                .name("Sondrio trail")
                .duration(50)
                .build();

        doRequest(MockMvcRequestBuilders.put(API_URL_UPDATE), rideToUpdate)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(saved.getId().toString()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Sondrio trail"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.duration").value(50));

        Ride rideToBeChecked = rideRepository.getSingleRide(saved.getId());
        Assertions.assertThat(rideToBeChecked.getName()).isEqualTo("Sondrio trail");
        Assertions.assertThat(rideToBeChecked.getDuration()).isEqualTo(50);
    }

    @Test
    @SneakyThrows
    void shouldUpdateBatch() {
        Ride rideAlbosaggia = Ride.builder()
                .name("Albosaggia trail")
                .duration(120)
                .build();
        Ride saved = rideRepository.createRideWithReturnBody(rideAlbosaggia);

        Ride rideToUpdate = Ride.builder()
                .id(saved.getId())
                .name("Sondrio trail")
                .duration(50)
                .build();
        Ride saved2 = rideRepository.createRideWithReturnBody(rideToUpdate);

        doRequest(MockMvcRequestBuilders.put(API_URL_UPDATE_BATCH), null)
                .andExpect(MockMvcResultMatchers.status().isOk());

        Ride rideUpdated1 = rideRepository.getSingleRide(saved.getId());
        Assertions.assertThat(rideUpdated1.getDuration()).isEqualTo(12);
        Ride rideUpdated2 = rideRepository.getSingleRide(saved2.getId());
        Assertions.assertThat(rideUpdated2.getDuration()).isEqualTo(12);
    }

}
