package com.example.springjdbc;

import com.example.springjdbc.repository.RideRepository;
import lombok.SneakyThrows;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

class RideControllerTransactionIT extends BaseSpringIT {
    private static final String API_URL_TRANSACTION = "/api/v1/testTransaction";

    @Autowired
    RideRepository rideRepository;

    @Test
    @SneakyThrows
    void shouldRollbackOnTransaction() {
        doRequest(MockMvcRequestBuilders.put(API_URL_TRANSACTION), null)
                .andExpect(MockMvcResultMatchers.status().isOk());
        Assertions.assertThat(rideRepository.getRidesWithExternalRowMapper()).isEmpty();
    }

}
