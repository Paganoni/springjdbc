package com.example.springjdbc.service;

import com.example.springjdbc.model.Ride;

import java.util.List;
import java.util.UUID;

public interface RideService {

    List<Ride> getRidesWithRowMapper();

    void createRideWithUpdateMethod(Ride ride);

    UUID createRideWithSimpleJdbcInsert(Ride ride);

    UUID createRideWithPreparedStatement(Ride ride);

    Ride createRideWithReturnBody(Ride ride);

    Ride getSingleRide(UUID id);

    Ride updateRide(Ride ride);

    void batchUpdate();

    void deleteRide(UUID id);

    void deleteRideNamedParameter(UUID id);

    void testException();

    void testTransaction();
}
