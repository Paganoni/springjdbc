package com.example.springjdbc.service;

import com.example.springjdbc.model.Ride;
import com.example.springjdbc.repository.RideRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service("rideService")
@RequiredArgsConstructor
public class RideServiceImpl implements RideService {

    private final RideRepository rideRepository;

    @Override
    public List<Ride> getRidesWithRowMapper() {
        return rideRepository.getRidesWithRowMapperInInnerClass();
    }

    @Override
    public void createRideWithUpdateMethod(Ride ride) {
        rideRepository.createRideWithUpdateMethod(ride);
    }

    @Override
    public UUID createRideWithSimpleJdbcInsert(Ride ride) {
        return rideRepository.createRideWithSimpleJdbcInsert(ride);
    }

    @Override
    public UUID createRideWithPreparedStatement(Ride ride) {
        return rideRepository.createRideWithPreparedStatement(ride);
    }

    @Override
    public Ride createRideWithReturnBody(Ride ride) {
        return rideRepository.createRideWithReturnBody(ride);
    }

    @Override
    public Ride getSingleRide(UUID id) {
        return rideRepository.getSingleRide(id);
    }

    @Override
    public Ride updateRide(Ride ride) {
        return rideRepository.updateRide(ride);
    }

    @Override
    public void batchUpdate() {
        List<Ride> rides = rideRepository.getRidesWithExternalRowMapper();
        List<Object[]> pairs = rides.stream().map(ride -> {
            return new Object[]{12, ride.getId()};
        }).collect(Collectors.toList());
        rideRepository.updateRides(pairs);

    }

    @Override
    public void deleteRide(UUID id) {
        rideRepository.deleteRide(id);
    }

    @Override
    public void deleteRideNamedParameter(UUID id) {
        rideRepository.deleteRideNamedParameter(id);
    }

    @Override
    public void testException() {
        rideRepository.testException();
    }

    /**
     * Viene simulata un'eccezione al termine di create e update per
     * verificare che non venga creato nulla, dato che siamo all'interno di una transazione
     */
    @Override
    @Transactional
    public void testTransaction() {
        Ride santaGiulia = Ride.builder()
                .name("Santa Giulia")
                .duration(5)
                .build();
        rideRepository.createRideWithPreparedStatement(santaGiulia);
        santaGiulia.setDuration(12);
        rideRepository.updateRide(santaGiulia);
        throw new DataAccessException("Simulated error") {
        };
    }
}
