package com.example.springjdbc.controller;

import com.example.springjdbc.model.Ride;
import com.example.springjdbc.service.RideService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class RideController {

	private final RideService rideService;

	/**
	 * CREATE
	 * putRideWithUpdateMethod -> utilizzo del metodo jdbcTemplate.update per gestire l'inserimento con insert SQL
	 * putRideWithPreparedStatement -> utilizzo del prepared statement, più ORM style
	 * putRideWithPreparedStatement -> utilizzo di prepared statement, più ORM style
	 * postRideWithReturnBody -> create + return object
	 */

	@RequestMapping(value = "/ride/update-method", method = RequestMethod.PUT)
	public @ResponseBody
	void putRideWithUpdateMethod(@RequestBody Ride ride) {
		rideService.createRideWithUpdateMethod(ride);
	}

	//Put because not returning any object
	@RequestMapping(value = "/ride/prepared-statement", method = RequestMethod.PUT)
	public @ResponseBody
	UUID putRideWithPreparedStatement(@RequestBody Ride ride) {
		return rideService.createRideWithPreparedStatement(ride);
	}

	@RequestMapping(value = "/ride/jdbc-insert", method = RequestMethod.PUT)
	public @ResponseBody
	UUID putRideWithSimpleJdbcInsert(@RequestBody Ride ride) {
		return rideService.createRideWithSimpleJdbcInsert(ride);
	}

	@RequestMapping(value = "/ride/with-return", method = RequestMethod.POST)
	public @ResponseBody
	Ride postRideWithReturnBody(@RequestBody Ride ride) {
		return rideService.createRideWithReturnBody(ride);
	}

	/**
	 * READ
	 * getSingleRide -> read di un oggetto e map del risultato con RomMapper
	 * getRidesWithRowMapper -> read di più oggetti e map sempre con rowMapper
	 */
	@RequestMapping(value = "/rides/{id}", method = RequestMethod.GET)
	public @ResponseBody
	Ride getSingleRide(@PathVariable final UUID id) {
		return rideService.getSingleRide(id);
	}
	@RequestMapping(value = "/rides/row-mapper", method = RequestMethod.GET)
	public @ResponseBody
	List<Ride> getRidesWithRowMapper() {
		return rideService.getRidesWithRowMapper();
	}

	/**
	 * UPDATE
	 * updateRide -> singolo update
	 * batchUpdate .-> batch update
	 */
	@RequestMapping(value = "/ride", method = RequestMethod.PUT)
	public @ResponseBody
	Ride updateRide(@RequestBody final Ride ride) {
		return rideService.updateRide(ride);
	}

	@RequestMapping(value = "/batch", method = RequestMethod.PUT)
	public @ResponseBody
	void batchUpdate() {
		rideService.batchUpdate();
	}

	/**
	 * DELETE
	 * deleteRide -> delete semplice
	 * deleteRideNamedParameter ->
	 */
	@RequestMapping(value = "/rides/{id}", method = RequestMethod.DELETE)
	public @ResponseBody
	void deleteRide(@PathVariable final UUID id) {
		rideService.deleteRide(id);
	}
	@RequestMapping(value = "/rides/{id}/namedParameter", method = RequestMethod.DELETE)
	public @ResponseBody
	void deleteRideNamedParameter(@PathVariable final UUID id) {
		rideService.deleteRideNamedParameter(id);
	}

	/**
	 * EXCEPTION
	 */
	@RequestMapping(value = "/testException", method = RequestMethod.PUT)
	public @ResponseBody
	void getException() {
		rideService.testException();
	}

	/**
	 * TRANSACTION
	 */
	@RequestMapping(value = "/testTransaction", method = RequestMethod.PUT)
	public @ResponseBody
	void getTransactionTest() {
		rideService.testTransaction();
	}

}
