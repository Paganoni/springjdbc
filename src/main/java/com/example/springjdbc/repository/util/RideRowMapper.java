package com.example.springjdbc.repository.util;

import com.example.springjdbc.model.Ride;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class RideRowMapper implements RowMapper<Ride> {
    @Override
    public Ride mapRow(ResultSet rs, int rowNum) throws SQLException {
        return Ride.builder()
                .id(rs.getObject("id", java.util.UUID.class))
                .name(rs.getString("name"))
                .duration(rs.getInt("duration"))
                .build();
    }
}
