package com.example.springjdbc.repository;

import com.example.springjdbc.model.Ride;

import java.util.List;
import java.util.UUID;

public interface RideRepository {

    List<Ride> getRidesWithRowMapperInInnerClass();

    //get con anonymous inner class; rowMapper usato solo in questo medoto
    List<Ride> getRidesWithExternalRowMapper();

    void createRideWithUpdateMethod(Ride ride);

    UUID createRideWithPreparedStatement(Ride ride);

    //SimpleJdbcInsert si può mettere a fattore comune per ogni tabella e istanziare una sola volta
    UUID createRideWithSimpleJdbcInsert(Ride ride);

    void deleteAll();

    Ride createRideWithReturnBody(Ride ride);

    Ride getSingleRide(UUID id);

    Ride updateRide(Ride ride);

    void updateRides(List<Object[]> pairs);

    void deleteRide(UUID id);

    void deleteRideNamedParameter(UUID id);

    void testException();
}
