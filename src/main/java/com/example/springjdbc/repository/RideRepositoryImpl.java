package com.example.springjdbc.repository;

import com.example.springjdbc.model.Ride;
import com.example.springjdbc.repository.util.RideRowMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Repository("rideRepository")
@RequiredArgsConstructor
public class RideRepositoryImpl implements RideRepository {
    private final JdbcTemplate jdbcTemplate;
    private final RideRowMapper rideRowMapper;


    /**
     * Viene utilizzato il metodo jdbcTemplate.queryForObject che permette di specificare una select e un RowMapper.
     * Il RowMapper può essere specificato come anonymous class, con un codice più complicato da leggere, oppure con un RowMapper
     * esterno referenziato nella classe e che viene passato come parametro.
     */
    @Override
    public List<Ride> getRidesWithExternalRowMapper() {
        return jdbcTemplate.query("select * from ride", rideRowMapper);
    }

    /**
     * Gestisce la creazione con una singola query di insert SQL
     */
    @Override
    public void createRideWithUpdateMethod(Ride ride) {
        int numberOfCreatedObjects = jdbcTemplate.update("insert into ride (name, duration) values (?,?)", ride.getName(), ride.getDuration());
        if (numberOfCreatedObjects == 0) {
            throw new RuntimeException("Item not inserted");
        }
    }

    /**
     * Gestisce la creazione con PreparedStatement, che astrae un po' il concetto di query e lo rende più simile a un ORM,
     * dato che stiamo lavorando con un oggetto che ingloba la vera e propria QUERY, permette di settare il tipo di valori da passare e
     * permette di ritornare l'UUID creato.
     */
    @Override
    public UUID createRideWithPreparedStatement(Ride ride) {
        final String INSERT_SQL = "insert into ride (name, duration) values (?,?)";

        //Oggetto dove viene salvata la key.
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(
                new PreparedStatementCreator() {
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        //PreparedStatement arriva da java.sql, che permette di ritornare l'id
                        PreparedStatement ps =
                                connection.prepareStatement(INSERT_SQL, new String[]{"id"});
                        ps.setString(1, ride.getName());
                        ps.setInt(2, ride.getDuration());
                        return ps;
                    }
                },
                keyHolder);
        return keyHolder.getKeyAs(UUID.class);
    }

    /**
     * Creazione con una maniera molto più ORM style, dato che stiamo lavorando con un vero e proprio oggetto e non abbiamo una insert SQL specificata.
     * In questo caso abbiamo l’oggetto SimpleJdbcInsert, che volendo si può anche istanziare una sola volta per tabella e poi si possono settare i valori di
     * volta in volta in base all’esigenza per il salvataggio su DB.
     */
    @Override
    public UUID createRideWithSimpleJdbcInsert(Ride ride) {
        //struttura
        SimpleJdbcInsert insert = new SimpleJdbcInsert(jdbcTemplate);
        insert.setGeneratedKeyName("id");
        List<String> columns = new ArrayList<>();
        columns.add("name");
        columns.add("duration");
        insert.setTableName("ride");
        insert.setColumnNames(columns);

        //valori
        Map<String, Object> data = new HashMap<>();
        data.put("name", ride.getName());
        data.put("duration", ride.getDuration());

        //inserty senza jdbcTemplate.update, ma con metodi appositi che ritonano la key
        KeyHolder keyHolder = insert.executeAndReturnKeyHolder(data);
        return keyHolder.getKeyAs(UUID.class);
    }

    /**
     * Viene creata la riga e ritornato l'oggetto solamente con una query di select aggiuntiva
     */
    @Override
    public Ride createRideWithReturnBody(Ride ride) {
        UUID id = createRideWithPreparedStatement(ride);
        return getSingleRide(id);
    }

    /**
     * RowMapper esterno
     */
    @Override
    public Ride getSingleRide(UUID id) {
        return jdbcTemplate.queryForObject("select * from ride where id = ?", rideRowMapper, id);
    }

    /**
     * RowMapper con anonymous inner class e con return di più oggetti.
     */
    @Override
    public List<Ride> getRidesWithRowMapperInInnerClass() {
       return jdbcTemplate.query("select * from ride", new RowMapper<Ride>() {
            @Override
            public Ride mapRow(ResultSet rs, int rowNum) throws SQLException {
                return Ride.builder()
                        .id(rs.getObject("id", java.util.UUID.class))
                        .name(rs.getString("name"))
                        .duration(rs.getInt("duration"))
                        .build();
            }
        });
        //alternativa con rowMapper esterno
        //return jdbcTemplate.query("select * from ride", rideRowMapper);
    }

    @Override
    public void deleteAll() {
        jdbcTemplate.update("delete from ride");
    }

    @Override
    public Ride updateRide(Ride ride) {
        jdbcTemplate.update("update ride set name = ?, duration = ? where id = ?",
                ride.getName(), ride.getDuration(), ride.getId());
        return ride;
    }

    /**
     * prende in input la query e una lista di array oggetti.
     * Questi oggetti altro non contengono i valori dei campi da modificare nella
     * query di update.
     * Ovviamente conta l’ordine dei valori inseriti nell’array, dato che verranno
     * associati alla query.

     */
    @Override
    public void updateRides(List<Object[]> pairs) {
        jdbcTemplate.batchUpdate("update ride set duration = ? where id = ?", pairs);
    }

    @Override
    public void deleteRide(UUID id) {
        jdbcTemplate.update("delete from ride where id = ?", id);
    }

    /**
     * Viene gestita la delete utilizzando NamedParameterJdbcTemplate.
     * Questo oggetto prende in input una mappa con chiave-valore (nome del campo e valori).
     * Il vantaggio è che in questo modo si possono passare molto valori.
     * Questo oggetto, poi, espone dei metodi, tipo update, per aggiornare/cancellare
     * righe su DB.
     */
    @Override
    public void deleteRideNamedParameter(UUID id) {
        NamedParameterJdbcTemplate namedTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        Map <String, Object> paramMap = new HashMap<>();
        paramMap.put("id", id);
        namedTemplate.update("delete from ride where id = :id", paramMap);
    }

    /**
     * Metodo che lancerà un'eccezione, dato che stiamo cancellando dati da una tabella non esistente
     */
    @Override
    public void testException() {
        jdbcTemplate.update("delete from tabellaNonEsistente where id = ?", 12);
    }

}
