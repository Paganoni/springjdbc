package com.example.springjdbc.util;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class ErrorHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ServiceErrorMessage> handle(RuntimeException ex) {
        ServiceErrorMessage error = ServiceErrorMessage.builder()
                .code(HttpStatus.OK.value())
                .message(ex.getMessage())
                .build();
        return new ResponseEntity<>(error, HttpStatus.OK);
    }

}
